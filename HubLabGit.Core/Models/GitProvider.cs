namespace HubLabGit.Core.Models
{
    public enum GitProvider
    {
        GitLab,
        GitHub
    }
}