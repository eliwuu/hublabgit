namespace HubLabGit.Core.Models
{
    public class User
    {
        public string Name {get;set;}
        public string AccessToken {get;set;}

        public bool Authorization {get;set;}
    }
}