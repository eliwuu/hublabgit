using Newtonsoft.Json;

namespace HubLabGit.Core.Models.Repo.Gitlab
{
    public class PublicRepository
    {
        [JsonProperty("id")]
        public int Id {get;set;}

        [JsonProperty("description")]
        public string Description {get;set;}

        [JsonProperty("name")]
        public string Name {get;set;}

        [JsonProperty("name_with_namespace")]
        public string NameWithNamespace {get;set;}

        [JsonProperty("path")]
        public string Path {get;set;}

        [JsonProperty("path_with_namespace")]
        public string PathWithNamespace {get;set;}

        [JsonProperty("created_at")]
        public string CreatedAt {get;set;}

        [JsonProperty("default_branch")]
        public string DefaultBranch {get;set;}

        [JsonProperty("tag_list")]
        public string[] TagList {get;set;}

        [JsonProperty("ssh_url_to_repo")]
        public string SshUrl {get;set;}

        [JsonProperty("http_url_to_repo")]
        public string HttpUrl {get;set;}

        [JsonProperty("web_url")]
        public string WebUrl {get;set;}

        [JsonProperty("readme_url")]
        public string ReadmeUrl {get;set;} 

        [JsonProperty("avatar_url")]
        public string AvatarUrl {get;set;}

        [JsonProperty("star_count")]
        public int StarCount {get;set;}

        [JsonProperty("forks_count")]
        public int ForksCount {get;set;}

        [JsonProperty("last_activity")]
        public string LastActivity {get;set;}

        [JsonProperty("namespace")]
        public NSpace NSpace {get;set;}
    }
    public class NSpace
    {
        [JsonProperty("id")]
        public int Id {get;set;}

        [JsonProperty("name")]
        public string Name {get;set;}

        [JsonProperty("path")]
        public string Path {get;set;}

        [JsonProperty("kind")]
        public string Kind {get;set;}
        
        [JsonProperty("full_path")]
        public string FullPath {get;set;}
        
        [JsonProperty("parent_id")]
        public string ParentId {get;set;}
        
        // user avatar
        [JsonProperty("avatar_url")]
        public string AvatarUrl {get;set;}
        
        // user acc web
        [JsonProperty("web_url")]
        public string WebUrl {get;set;}
    }
}