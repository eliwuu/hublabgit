using Newtonsoft.Json;

namespace HubLabGit.Core.Models.Repo.Gitlab
{
    public class Permissions
    {
        [JsonProperty("project_access")]
        public ProjectAccess ProjectAccess { get; set; }

        [JsonProperty("group_access")]
        // FIXME: GroupAccess is an object
        public string GroupAccess { get; set; }
    }
}