using Newtonsoft.Json;

namespace HubLabGit.Core.Models.Repo.Gitlab
{
    public class TreeElement
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("type")]
        public string ElementType { get; set; }
        [JsonProperty("path")]
        public string Path { get; set; }
        [JsonProperty("mode")]
        public string Mode { get; set; }
    }
}