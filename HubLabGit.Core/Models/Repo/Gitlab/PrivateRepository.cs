using Newtonsoft.Json;

namespace HubLabGit.Core.Models.Repo.Gitlab
{
    public class PrivateRepository : PublicRepository
    {
        [JsonProperty("_links")]
        public Links Links {get;set;}

        [JsonProperty("empty_repo")]
        public bool EmptyRepo {get;set;}

        [JsonProperty("archived")]
        public bool Archived {get;set;}

        [JsonProperty("visibility")]
        public string Visibility {get;set;}

        [JsonProperty("owner")]
        public Owner Owner {get;set;} 

        [JsonProperty("resolve_outdated_diff_discussions")]
        public bool ResolvedOutdatedDiffDisscussions {get;set;}
        [JsonProperty("container_registry_enabled")]
        public bool ContainterRegistryEnabled {get;set;}
        [JsonProperty("issues_enabled")]
        public bool IssuesEnabled {get;set;}
        [JsonProperty("wiki_enabled")]
        public bool WikiEnabled {get;set;}
        [JsonProperty("merge_requests_enabled")]
        public bool MergeRequestsEnabled {get;set;}

        [JsonProperty("jobs_enabled")]
        public bool JobsEnabled {get;set;}
        [JsonProperty("snippets_enabled")]
        public bool SnippetsEnabled {get;set;}
        [JsonProperty("shared_runners_enabled")]
        public bool SharedRunnersEnabled {get;set;}
        [JsonProperty("lfs_enabled")]
        public bool LfsEnabled {get;set;}
        [JsonProperty("creator_id")]
        public int CreatorId {get;set;}
        [JsonProperty("import_status")]
        public string ImportStatus {get;set;}
        [JsonProperty("import_error")]
        public string ImportError {get;set;}
        [JsonProperty("open_issues_count")]
        public int OpenIssuesCount {get;set;}
        [JsonProperty("runners_token")]
        public string RunnersToken {get;set;}
        [JsonProperty("ci_default_git_depth")]
        public int? CiDefaultGitDepth {get;set;}
        [JsonProperty("shared_with_groups")]
        public string[] SharedWithGroups {get;set;}
        [JsonProperty("only_allow_merge_if_pipeline_succeeds")]
        public bool OnlyAllowMergeIfPipelineSucceeds {get;set;}
        [JsonProperty("request_access_enabled")]
        public bool RequestAccessEnabled {get;set;}
        [JsonProperty("only_allow_merge_if_all_discussions_are_resolved")]
        public bool OnlyAllowMergeIfAllDiscussionsAreResolved {get;set;}
        [JsonProperty("printing_merge_requests_link_enabled")]
        public bool PrintingMergeRequestsLinkEnabled {get;set;}
        [JsonProperty("merge_method")]
        public string MergeMethod {get;set;}
        [JsonProperty("external_authorization_classification_label")]
        public string ExternalAuthorizationClassificationLabel {get;set;}
        [JsonProperty("permissions")]
        public Permissions Permissions {get;set;}

        [JsonProperty("approvals_before_merge")]
        public int ApprovalsBeforeMerge {get;set;}
        [JsonProperty("mirror")]
        public bool Mirror {get;set;}
        [JsonProperty("packages_enabled")]
        public bool PackagesEnabled {get;set;}
    }
}