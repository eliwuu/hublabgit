using Newtonsoft.Json;

namespace HubLabGit.Core.Models.Repo.Gitlab
{
    public class Branch
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("commit")]
        public Commit Commit { get; set; }
        [JsonProperty("merged")]
        public bool Merged { get; set; }
        [JsonProperty("protected")]
        public bool Protected { get; set; }
        [JsonProperty("developers_can_push")]
        public bool DevelopersCanPush { get; set; }
        [JsonProperty("developers_can_merge")]
        public bool DevelopersCanMerge { get; set; }
        [JsonProperty("can_push")]
        public bool CanPush { get; set; }
        [JsonProperty("default")]
        public bool Default {get;set;}
    }
}