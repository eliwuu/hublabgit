namespace HubLabGit.Core.Models.Repo
{
    public class Repository
    {
        public string Name {get;set;}
        public GitProvider GitProvider {get;set;}
    }
}