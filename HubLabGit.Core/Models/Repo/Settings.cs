namespace HubLabGit.Core.Models.Repo
{
    public class Settings
    {
        private readonly User _user;
        private readonly GitProvider _gitProvider;
        private readonly Repository _repo;

        public string Username { get; }
        public string Reponame { get; }
        public string AccessToken { get; }
        public bool Authorization { get; }
        public GitProvider GitProvider { get; }
        public Settings(User user, Repository repo)
        {
            _user = user;
            _gitProvider = repo.GitProvider;
            _repo = repo;

            Username = _user.Name;
            AccessToken = _user.AccessToken;
            Authorization = _user.Authorization;
            Reponame = _repo.Name;
            GitProvider = _gitProvider;
        }
    }
}