namespace HubLabGit.Core.Models
{
    public class GitlabPaths : BasePaths
    {
        public string Id {get;set;}
        public string Api = "https://gitlab.com/api/v4/projects/";

        public string ListTree {get;}
        public string GetArchive {get;}
        public string Branches {get;}

        public GitlabPaths(int id)
        {
            Id = id.ToString();
            ListTree = Api + Id + "/repository/tree";
            GetArchive = Api + Id + "/repository/archive.zip";
            Branches = Api + Id + "/repository/branches";
        }
    }

    public class BasePaths
    {
        public string GitlabApi {get;} = "https://gitlab.com/api/v4/projects/";
        public string GithubApi {get;} = "";
    }
}