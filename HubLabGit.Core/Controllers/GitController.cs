using System.Net.Http;
using HubLabGit.Core.Models;
using HubLabGit.Core.Models.Repo;

namespace HubLabGit.Core.Controllers
{
    public class GitController
    {
        public string ApiPath { get; private set; }
        public string Delimiter { get; private set; }

        public HttpClient http;

        public GitProvider GitProvider { get; }

        public GitController(GitProvider gitProvider)
        {
            http = new HttpClient();

            SetApiPath(gitProvider);
        }

        private void SetApiPath(GitProvider gitProvider)
        {
            switch (gitProvider)
            {
                case GitProvider.GitLab:
                    ApiPath = new BasePaths().GitlabApi;
                    Delimiter = "%2F";
                    break;
                case GitProvider.GitHub:
                    ApiPath = new BasePaths().GithubApi;
                    throw new System.NotImplementedException();
            }
        }
    }
}