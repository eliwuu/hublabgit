using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using HubLabGit.Core.Models;
using HubLabGit.Core.Models.Repo;
using HubLabGit.Core.Models.Repo.Gitlab;
using Newtonsoft.Json;

namespace HubLabGit.Core.Controllers
{
    public class GitlabController : GitController
    {
        public PublicRepository PublicRepository { get; private set; }
        public PrivateRepository PrivateRepository { get; private set; }

        private GitlabPaths paths;

        private readonly Settings _settings;

        public GitlabController(Settings settings) : base(settings.GitProvider)
        {
            _settings = settings;
            PrivateRepository = new PrivateRepository();
        }

        public async Task Init()
        {
            try
            {
                var path = GetInitPath();
        
                var jsonString = await Get(path);

                var deserialize = JsonConvert.DeserializeObject<PrivateRepository>(jsonString);
                PrivateRepository = deserialize;

                paths = new GitlabPaths(PrivateRepository.Id);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }
        public async Task GetArchive()
        {
            var stream = await GetStream();

            await SaveToFile(stream, string.Empty);
        }

        public async Task GetArchive(string destination)
        {
            var stream = await GetStream();

            await SaveToFile(stream, destination);
        }
        public async Task GetArchiveBranch(string destination, string sha)
        {
            var stream = await GetStream(sha);

            await SaveToFile(stream, destination);
        }

        public async Task<string> GetListTree()
        {
            var listTree = await Get(paths.ListTree);

            return listTree;
        }

        public async Task<string> GetBranches()
        {
            var branches = await Get(paths.Branches);
            
            return branches;
        }

        private async Task<string> GetPublicRepo()
        {
            var path = GetInitPath();

            var response = await http.GetAsync(path);

            try
            {
                response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();

                return responseBody;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private async Task<string> Get(string path)
        {
            if (string.IsNullOrEmpty(_settings.AccessToken))
                throw new System.Exception("Access Token is not specfied (null)");

            var request = new HttpRequestMessage()
            {
                RequestUri = new System.Uri(path),
                Method = HttpMethod.Get
            };

            request.Headers.Add("PRIVATE-TOKEN", _settings.AccessToken);

            var response = await http.SendAsync(request);

            try
            {
                // response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStringAsync();

                return responseBody;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private async Task<Stream> GetStream()
        {
            if (string.IsNullOrEmpty(_settings.AccessToken))
                throw new System.Exception("Access Token is not specfied (null)");

            var request = new HttpRequestMessage()
            {
                RequestUri = new System.Uri(paths.GetArchive),
                Method = HttpMethod.Get
            };

            request.Headers.Add("PRIVATE-TOKEN", _settings.AccessToken);

            var response = await http.SendAsync(request);

            try
            {
                // response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStreamAsync();

                return responseBody;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private async Task<Stream> GetStream(string sha)
        {
            if (string.IsNullOrEmpty(_settings.AccessToken))
                throw new System.Exception("Access Token is not specfied (null)");

            var request = new HttpRequestMessage()
            {
                RequestUri = new System.Uri(paths.GetArchive + "?sha=" + sha),
                Method = HttpMethod.Get
            };

            request.Headers.Add("PRIVATE-TOKEN", _settings.AccessToken);

            var response = await http.SendAsync(request);

            try
            {
                // response.EnsureSuccessStatusCode();
                var responseBody = await response.Content.ReadAsStreamAsync();

                return responseBody;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return null;
            }
        }

        private async Task SaveToFile(Stream stream, string destination)
        {
            var dest = string.IsNullOrEmpty(destination) ? _settings.Reponame : destination + $"/{_settings.Reponame}";

            using(var fs = new FileStream(dest + ".zip", FileMode.Create, FileAccess.Write))
            {
                await stream.CopyToAsync(fs);
            }
        }
        private string GetInitPath()
        {
            return $"{ApiPath}{_settings.Username}{Delimiter}{_settings.Reponame}";
        }
    }
}