//
//  GitlabPaths.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct GitlabPaths {
    let api = BasePaths().GitlabApi
    
    var id: String
    var listTree: String
    var getArchive: String
    var branches: String
    init(id: Int) {
        self.id = String(id)
        self.listTree = api + self.id + "/repository/tree"
        self.getArchive = api + self.id + "/repository/archive.zip"
        self.branches = api + self.id + "/repository/branches"
    }
    
}
