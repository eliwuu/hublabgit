//
//  BasePaths.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct BasePaths
{
    let GitlabApi = "https://gitlab.com/api/v4/projects/"
    let GithubApi = ""
}
