//
//  PrivateRepository.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class PrivateRepository : PublicRepository {
    var links: Links?
//    var links: String?
    var emptyRepo: Bool?
    var archived: Bool?
    var visibility: Bool?
    var owner: Owner?
    var resolvedOutdatedDiffDiscussions: Bool?
    var containerRegistryEnabled: Bool?
    var issuesEnabled: Bool?
    var wikiEnabled: Bool?
    var mergeRequestEnabled: Bool?
    var jobsEnabled: Bool?
    var snippetsEnabled: Bool?
    var sharedRunnersEnabled: Bool?
    var lfsEnabled: Bool?
    var creatorId: Int?
    var importStatus: String?
    var importError: String?
    var openIssuesCount: Int?
    var runnersToken: String?
    var ciDefaultGitDepth: Int?
    var sharedWithGroups: [String]?
    var onlyAllowMergeIfPipelineSucceeds: Bool?
    var requestAccessEnabled: Bool?
    var onlyAllowMergeIfAllDiscussionsAreResolved: Bool?
    var printingMergeRequestsLinkEnabled: Bool?
    var mergeMethod: String?
    var externalAuthorizationClassificationLabel: String?
    var permissions: Permissions?
    var approvalsBeforeMerge: Int?
    var mirror: Bool?
    var packagesEnabled: Bool?
}
