//
//  ProjectAccess.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct ProjectAccess {
    var accessLevel: Int?
    var notificationLevel: Int?
}
