//
//  Permissions.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct Permissions {
    var projectAccess: String?
    var groupAccess: String?
//    var projectAccess: ProjectAccess?
//    var groupAccess: GroupAccess?
}
