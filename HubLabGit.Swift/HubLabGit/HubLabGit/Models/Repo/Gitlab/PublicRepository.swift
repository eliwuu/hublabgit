//
//  PublicRepository.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class PublicRepository {
    var id: Int?
    var description: String?
    var name: String?
    var nameWithNamespace: String?
    var path: String?
    var pathWithNamespace: String?
    var createdAt: String?
    var defaultBranch: String?
    var tagList: [String]?
    var sshUrlToRepo: String?
    var httpUrlToRepo: String?
    var webUrl: String?
    var readmeUrl: String?
    var avatarUrl: String?
    var starCount: Int?
    var forksCount: Int?
    var lastActivity: String?
    var nameSpace: NameSpace?
}
