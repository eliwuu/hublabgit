//
//  Owner.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct Owner {
    var id: Int?
    var name: String?
    var username: String?
    var state: String?
    var avatarUrl: String?
    var webUrl: String?
}
