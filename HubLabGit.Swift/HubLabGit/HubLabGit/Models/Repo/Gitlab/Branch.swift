//
//  Branch.swift
//  HubLabGit
//
//  Created by Marek Wilk on 11/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class Branch {
    var name: String?
    var commit: Commit?
    var merged: Bool?
    var protected: Bool?
    var developersCanPush: Bool?
    var developersCanMerge: Bool?
    var canPush: Bool?
    var defaultBranch: Bool?
}
