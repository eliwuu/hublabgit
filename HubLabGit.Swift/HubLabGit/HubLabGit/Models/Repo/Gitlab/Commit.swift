//
//  Commit.swift
//  HubLabGit
//
//  Created by Marek Wilk on 11/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class Commit {
    var id: String?
    var shortId: String?
    var createdAt: String?
    var parentIds: String?
    var title: String?
    var message: String?
    var authorName: String?
    var authorEmail: String?
    var authoredDate: String?
    var commiterName: String?
    var commiterEmail: String?
    var commitedDate: String?
}
