//
//  Links.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct Links {
    var selfLink: String?
    var issues: String?
    var mergeRequests: String?
    var repoBranches: String?
    var labels: String?
    var events: String?
    var members: String?
}
