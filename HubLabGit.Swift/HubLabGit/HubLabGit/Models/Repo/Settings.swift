//
//  Settings.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class Settings {
    var user: User
    var gitProvider: GitProvider
    var repository: Repository
    
    var username: String
    var reponame: String
    var accessToken: String
    var authorization: Bool
    
    init(user: User, repository: Repository) {
        self.user = user
        self.repository = repository
        self.gitProvider = repository.gitProvider
        
        self.username = user.name
        self.accessToken = user.accessToken
        self.reponame = repository.name
        self.authorization = user.authorization
    }
}
