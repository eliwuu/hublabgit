//
//  User.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

struct User {
    var name: String
    var accessToken: String
    var authorization: Bool
    
    init(name: String, accessToken: String, authorization: Bool) {
        self.name = name
        self.accessToken = accessToken
        self.authorization = authorization
    }
}
