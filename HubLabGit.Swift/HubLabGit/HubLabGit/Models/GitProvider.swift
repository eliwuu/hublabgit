//
//  GitProvider.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

enum GitProvider {
    case Gitlab
    case Github
}
