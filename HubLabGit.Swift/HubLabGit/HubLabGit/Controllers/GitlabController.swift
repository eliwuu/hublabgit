//
//  GitlabController.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation
import SwiftyJSON

class GitlabController : GitController {
    var settings: Settings
    var privateRepo: PrivateRepository = PrivateRepository()
    var paths: GitlabPaths?
    
    init(settings: Settings) {
        self.settings = settings
        super.init(gitProvider: settings.gitProvider)
    }
    
    func initialize() {
        let path = getInitPath()
        getCmd(path: path)
        paths = GitlabPaths(id: privateRepo.id!)
    }
    
    func getRepoFromBranch(sha: String) {
        let archivePath = paths!.getArchive + "?sha=" + sha
        let url = URL(string: archivePath)
        
        let response = getGeneric(path: url!)
        
        saveRepository(data: response)
    }
    
    func getBranches() -> [Branch] {
        let branchesURL = URL(string: paths!.branches)
        let response = getGeneric(path: branchesURL!)
        
        let json = try? JSON(data: response)
        
        let branches = deserializeBranch(jsonData: json!)
        
        return branches
    
    }
    
    private func getGeneric(path: URL) -> Data {
        let runLoop = CFRunLoopGetCurrent()
        
        var resp: Data?
        
        var request = URLRequest(url: path)
        request.httpMethod = "GET"
        request.setValue(settings.user.accessToken, forHTTPHeaderField: "PRIVATE-TOKEN")
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            
            guard let data = data, error == nil else {
                print("error: \(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("\(httpStatus.statusCode), response: \(String(describing: response))")
            }
            
            resp = data
            
            CFRunLoopStop(runLoop)
        }
        
        task.resume()
        CFRunLoopRun()
//        print("task.response: \(String(describing: task.response))")
        
        return resp!
    }
    
    private func getCmd(path: URL) {
        let runLoop = CFRunLoopGetCurrent()
        
        var request = URLRequest(url: path)
        request.httpMethod = "GET"
        request.setValue(settings.user.accessToken, forHTTPHeaderField: "PRIVATE-TOKEN")
        
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            guard let data = data, error == nil else {
                print("error: \(String(describing: error))")
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                print("status code should be 200, but is \(httpStatus.statusCode)")
                print("response: \(String(describing: response))")
            }
            
            self.deserializePrivateRepository(data: data)
//            self.saveRawJson(data: data)
        
            CFRunLoopStop(runLoop)
        }
        
        task.resume()
        CFRunLoopRun()
    }
    
    
    private func getInitPath() -> URL {
        let stringUrl = "\(apiPath)\(settings.username)\(delimiter)\(settings.reponame)"
        
        let url = URL(string: stringUrl)
        
        return url!
    }
    
    private func deserializeBranch(jsonData: JSON) -> [Branch] {
        let branchesJson = jsonData.arrayValue
        
        var branches: [Branch] = []
        
        for branch in branchesJson {
            let deserializeBranch = branch.dictionary
            let deserialize = Branch()
            deserialize.name = deserializeBranch!["name"]?.string
            
            if(deserialize.name == nil) {
                print("branch for whatever reason is nil");
            }
            
            let commitJson = deserializeBranch!["commit"]!.dictionary
            
            let commit = Commit()
            commit.id = commitJson!["id"]?.string
            commit.shortId = commitJson!["short_id"]?.string
            commit.createdAt = commitJson!["created_at"]?.string
            commit.parentIds = commitJson!["parent_ids"]?.string
            commit.title = commitJson!["title"]?.string
            commit.message = commitJson!["message"]?.string
            commit.authorName = commitJson!["author_name"]?.string
            commit.authorEmail = commitJson!["author_email"]?.string
            commit.authoredDate = commitJson!["authored_date"]?.string
            commit.commiterName = commitJson!["commiter_name"]?.string
            commit.commiterEmail = commitJson!["commiter_email"]?.string
            commit.commitedDate = commitJson!["commited_date"]?.string
            
            deserialize.commit = commit
            deserialize.merged = deserializeBranch!["merged"]?.boolValue
            deserialize.protected = deserializeBranch!["protected"]?.boolValue
            deserialize.developersCanPush = deserializeBranch!["developers_can_push"]?.boolValue
            deserialize.developersCanMerge = deserializeBranch!["developers_can_merge"]?.boolValue
            deserialize.canPush = deserializeBranch!["can_push"]?.boolValue
            deserialize.defaultBranch = deserializeBranch!["default"]?.boolValue
            
            branches.append(deserialize)
        }
        
        return branches
    }
    
    private func deserializePrivateRepository(data: Data) {
        let json = try? JSON(data: data)
        
        let id = json!["id"].intValue
        let description = json!["description"].string
        let name = json!["name"].string
        let nameWithNamespace = json!["name_with_namespace"].string
        let path = json!["path"].string
        let pathWithNamespace = json!["path_with_namespace"].string
        let createdAt = json!["created_at"].string
        let defaultBranch = json!["default_branch"].string
        let tagList = json!["tag_list"].arrayObject
        let sshUrl = json!["ssh_url_to_repo"].string
        let httpUrl = json!["http_url_to_repo"].string
        let webUrl = json!["web_url"].string
        let readmeUrl = json!["readme_url"].string
        let avatarUrl = json!["avatar_url"].string
        let starCount = json!["star_count"].intValue
        let lastActivity = json!["last_activity"].string
        let nameSpaceJson = json!["namespace"].dictionaryObject
        let linksJson = json!["_links"].dictionaryObject
        let emptyRepo = json!["empty_repo"].boolValue
        let archived = json!["archived"].boolValue
        let visibility = json!["visibility"].boolValue
        let ownerJson = json!["owner"].dictionaryObject
        let resolvedOutdated = json!["resolve_outdated_diff_discussions"].boolValue
        let containerRegistryEnabled = json!["container_registry_enabled"].boolValue
        let issuesEnabled = json!["issues_enabled"].boolValue
        let wikiEnabled = json!["wiki_enabled"].boolValue
        let mergeRequestEnabled = json!["merge_requests_enabled"].boolValue
        let jobsEnabled = json!["jobs_enabled"].boolValue
        let snippetsEnabled = json!["snippets_enabled"].boolValue
        let sharedRunnersEnabled = json!["shared_runners_enabled"].boolValue
        let lfsEnabled = json!["lfs_enabled"].boolValue
        let creatorId = json!["creator_id"].intValue
        let importStatus = json!["import_status"].string
        let importError = json!["import_error"].string
        let openIssuesCount = json!["open_issues_count"].intValue
        let runnersToken = json!["runners_token"].string
        
        let ciDefaultGitDepth = json!["ci_default_git_depth"].intValue
        let sharedWithGroups = json!["shared_with_groups"].arrayObject
        let onlyAllowMergeIfPipeLineSucceeds = json!["only_allow_merge_if_pipeline_succeeds"].boolValue
        let requestAccessEnabled = json!["request_access_enabled"].boolValue
        let onlyAllowMergeIfAllDiscussionsAreResolved = json!["only_allow_merge_if_all_discussions_are_resolved"].boolValue
        let printingMergeRequestsLinkEnabled = json!["printing_merge_requests_link_enabled"].boolValue
        let mergeMethod = json!["merge_method"].string
        let externalAuthorizationClassificationLabel = json!["external_authorization_classification_label"].string
        let permissionsJson = json!["permissions"].dictionaryObject
        let approvalsBeforeMerge = json!["approvals_before_merge"].intValue
        let mirror = json!["mirror"].boolValue
        let packagesEnabled = json!["pacakges_enabled"].boolValue
        
        var nameSpace = NameSpace()
        nameSpace.id = nameSpaceJson!["id"] as? Int
        nameSpace.name = nameSpaceJson!["name"] as? String
        nameSpace.path = nameSpaceJson!["path"] as? String
        nameSpace.kind = nameSpaceJson!["kind"] as? String
        nameSpace.parentId = nameSpaceJson!["parent_id"] as? String
        nameSpace.avatarUrl = nameSpaceJson!["avatar_url"] as? String
        nameSpace.webUrl = nameSpaceJson!["web_url"] as? String
        
        var links = Links()
        links.selfLink = linksJson!["self"] as? String ?? "empty"
        links.issues = linksJson!["issues"] as? String
        links.mergeRequests = linksJson!["merge_requests"] as? String
        links.repoBranches = linksJson!["repo_branches"] as? String
        links.labels = linksJson!["labels"] as? String
        links.events = linksJson!["events"] as? String
        links.members = linksJson!["members"] as? String
        
        var owner = Owner()
        owner.id = ownerJson!["id"] as? Int
        owner.name = ownerJson!["name"] as? String
        owner.username = ownerJson!["username"] as? String
        owner.state = ownerJson!["state"] as? String
        owner.avatarUrl = ownerJson!["avatar_url"] as? String
        owner.webUrl = ownerJson!["web_url"] as? String
        
        
        // todo: work on permissions
        var permissions = Permissions()
        permissions.projectAccess = permissionsJson!["project_access"] as? String
        permissions.groupAccess = permissionsJson!["group_access"] as? String
        
        privateRepo.id = id
        privateRepo.description = description
        privateRepo.name = name
        privateRepo.nameWithNamespace = nameWithNamespace
        privateRepo.path = path
        privateRepo.pathWithNamespace = pathWithNamespace
        privateRepo.createdAt = createdAt
        privateRepo.defaultBranch = defaultBranch
        privateRepo.tagList = tagList as? [String] ?? [""]
        privateRepo.sshUrlToRepo = sshUrl
        privateRepo.httpUrlToRepo = httpUrl
        privateRepo.webUrl = webUrl
        privateRepo.readmeUrl = readmeUrl
        privateRepo.avatarUrl = avatarUrl
        privateRepo.starCount = starCount
        privateRepo.lastActivity = lastActivity
        privateRepo.nameSpace = nameSpace
        privateRepo.links = links
        privateRepo.emptyRepo = emptyRepo
        privateRepo.archived = archived
        privateRepo.visibility = visibility
        privateRepo.owner = owner
        privateRepo.resolvedOutdatedDiffDiscussions = resolvedOutdated
        privateRepo.containerRegistryEnabled = containerRegistryEnabled
        privateRepo.issuesEnabled = issuesEnabled
        privateRepo.wikiEnabled = wikiEnabled
        privateRepo.mergeRequestEnabled = mergeRequestEnabled
        privateRepo.jobsEnabled = jobsEnabled
        privateRepo.snippetsEnabled = snippetsEnabled
        privateRepo.sharedRunnersEnabled = sharedRunnersEnabled
        privateRepo.lfsEnabled = lfsEnabled
        privateRepo.creatorId = creatorId
        privateRepo.importStatus = importStatus
        privateRepo.importError = importError
        privateRepo.openIssuesCount = openIssuesCount
        privateRepo.runnersToken = runnersToken
        privateRepo.ciDefaultGitDepth = ciDefaultGitDepth
        privateRepo.sharedWithGroups = sharedWithGroups as? [String] ?? [""]
        privateRepo.requestAccessEnabled = requestAccessEnabled
        privateRepo.onlyAllowMergeIfPipelineSucceeds = onlyAllowMergeIfPipeLineSucceeds
        privateRepo.printingMergeRequestsLinkEnabled = printingMergeRequestsLinkEnabled
        privateRepo.mergeMethod = mergeMethod
        privateRepo.onlyAllowMergeIfAllDiscussionsAreResolved = onlyAllowMergeIfAllDiscussionsAreResolved
        privateRepo.externalAuthorizationClassificationLabel = externalAuthorizationClassificationLabel
        privateRepo.permissions = permissions
        privateRepo.approvalsBeforeMerge = approvalsBeforeMerge
        privateRepo.mirror = mirror
        privateRepo.packagesEnabled = packagesEnabled
    }
    
    private func saveRepository(data: Data) {
        let file = "\(settings.reponame).zip"
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileUrl = dir.appendingPathComponent(file)
            
            do {
                try data.write(to: fileUrl)
            }
            catch {
                
            }
        }
    }
    
    private func saveRawJson(data: Data) {
        let file = "rawPrivateRepo.json"
        
        let content = String(data: data, encoding: .utf8)
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            
            let fileUrl = dir.appendingPathComponent(file)
            
            do {
                try content!.write(to: fileUrl, atomically: false, encoding: .utf8)
            }
            catch {
                
            }
        }
    }
}
