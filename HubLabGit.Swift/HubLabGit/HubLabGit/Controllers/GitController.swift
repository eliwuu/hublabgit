//
//  GitController.swift
//  HubLabGit
//
//  Created by Marek Wilk on 10/07/2019.
//  Copyright © 2019 Marek Wilk. All rights reserved.
//

import Foundation

class GitController {
    var apiPath: String
    var delimiter: String
    
    init(gitProvider: GitProvider) {
        switch gitProvider {
        case GitProvider.Gitlab:
            apiPath = BasePaths().GitlabApi
            delimiter = "%2F"
            break
        case GitProvider.Github:
            apiPath = BasePaths().GithubApi
            delimiter = "%2F"
            break
        }
    }
    
//    func HttpRequest(Url: String) {
//        let stringToUrl = URL(string: Url)
//    }
}
